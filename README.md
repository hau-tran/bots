==== WORK IN PROGRESS ====

# To use this service:

- You need to create a new task

```
POST /bots/tasks

{
    "name" : "Execute script",
    "pollTarget" : "http://localhost:8080/luz_compensation/version"
}


HTTP 200 OK

{
    "id" : 2342342,
    "links" : {
        "self": "/bots/tasks/2342342",
        "workspace" : "/bots/tasks/2342342/workspace",
    }
}
```


- You needs to upload the workspace archive (any ZIP file).

```
POST /bots/tasks/2342342/workspace
```





