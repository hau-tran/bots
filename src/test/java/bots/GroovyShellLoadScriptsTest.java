package bots;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Test;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

public class GroovyShellLoadScriptsTest {

	@Test
	public void should_able_to_load_scripts_then_compose_shell() throws Exception {
		Path coreScriptsFolder = Paths.get("src", "test", "resources", "com");

		GroovyScriptsCollector coreScriptsCollector = new GroovyScriptsCollector();
		Files.walkFileTree(coreScriptsFolder, coreScriptsCollector);
		
		Path insertScriptsFolder = Paths.get("src", "test", "resources", "other");
		GroovyScriptsCollector insertingScriptsCollector = new GroovyScriptsCollector();
		Files.walkFileTree(insertScriptsFolder, insertingScriptsCollector);

		Map<String, Object> contextVariables = new HashMap<>();
		GroovyClassLoader groovyClassLoader = new GroovyClassLoader(getClass().getClassLoader());
		GroovyShell shellContainingCommonScripts = new GroovyShell(groovyClassLoader, new Binding(contextVariables));

		StringWriter console = new StringWriter();
		PrintWriter out = new PrintWriter(console, true);
		
		contextVariables.put("out", out);
		contextVariables.put("resources", Paths.get("src", "test", "resources", "other").toAbsolutePath().toString());
		
		coreScriptsCollector.groovyScripts
			.stream()
			.map(p -> p.toFile())
			.map(Lambdas.Fx.of(f -> new GroovyCodeSource(f)))
			.forEach(g -> groovyClassLoader.parseClass(g));
		
		
		
		Map<String, Object> scriptVariables = insertingScriptsCollector.groovyScripts
			.stream()
			.filter(p -> !p.getFileName().toString().startsWith("main"))
			.map(p -> p.toFile())
			.map(Lambdas.Fx.of(p -> shellContainingCommonScripts.parse(p)))
			.collect(Collectors.toMap(this::scriptName, Function.identity()));
		
		
		contextVariables.put("scripts", scriptVariables);
			
		insertingScriptsCollector.groovyScripts
			.stream()
			.filter(p -> p.getFileName().toString().startsWith("main"))
			.map(p -> p.toFile())
			.map(Lambdas.Fx.of(f -> new GroovyCodeSource(f)))
			.forEach(g -> shellContainingCommonScripts.evaluate(g));
		
		System.out.println("Console!!!!");
		System.out.println(console.toString());
	}
	
	private String scriptName(Script s) {
		return s.getClass().getSimpleName();
	}
	
	static class Lambdas {
		
		public static interface Fx<T,R> extends Function<T, R> {
			
			public static <T,R> Function<T, R> of(Fx<T, R> functionThrowingException) {
				return functionThrowingException;
			}
			
			@Override
			public default R apply(T t) {
				try {
					return a(t);
				} catch (Exception any) {
					throw new RuntimeException(any);
				}
			}
			
			public R a(T t) throws Exception;
		}
	}
	
	
	static class GroovyScriptsCollector implements FileVisitor<Path> {

		private List<Path> groovyScripts = new ArrayList<>();
		
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			PathMatcher groovyExtension = FileSystems.getDefault().getPathMatcher("glob:*.groovy");
			if (groovyExtension.matches(file.getFileName())) {
				groovyScripts.add(file);
			}
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			return FileVisitResult.TERMINATE;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			return FileVisitResult.CONTINUE;
		}
		
	}
}
