package com.axonivy.scripting;

import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*
import groovy.json.*
import groovyx.net.http.HTTPBuilder

import java.nio.charset.StandardCharsets
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import org.yaml.snakeyaml.Yaml

public class RestClient {
	private static final String DATE_FORMAT_USING_IN_GROOVY = "dd.MM.yyyy";
	private static final String DATE_TIME_SHORT_FORMAT_CONFORM_TO_ISO_8601 = "yyyy-MM-dd";

	private String serviceContextPath;
	private String hostname;
	private String port;
	private String serviceURL;

	public RestClient(String serviceURL) {
			   this.serviceURL = serviceURL;
	}

	public RestClient(String hostname, String port, String serviceContextPath) {
		this.hostname = hostname;
		this.port = port;
		this.serviceContextPath = serviceContextPath;
		this.serviceURL = extractURL(hostname, port, serviceContextPath);
	}

	public List getList(String pathInput, LinkedHashMap queryJson) {
		List list;
		HTTPBuilder httpBuilder = new HTTPBuilder();
		httpBuilder.request(this.serviceURL, GET, JSON) { req ->
			uri.path = pathInput;
			uri.query = queryJson;
			requestContentType = JSON;
			headers.Accept = 'application/json;charset=utf-8';
			response.success = { resp, json ->
				list = json;
			}
		}
		return list;
	}

	public void delete(String pathInput) {
		HTTPBuilder httpBuilder = new HTTPBuilder();
		httpBuilder.request(this.serviceURL, DELETE, JSON) { req ->
			uri.path = pathInput;
			requestContentType = JSON;
			headers.Accept = 'application/json;charset=utf-8';
			response.success = { resp, json ->
				println "Deleted: " + pathInput;
			}
		}
	}

	public String getId(String pathInput, LinkedHashMap queryJson) {
		// correct format of params's value contains special characters
		queryJson.each { key, value ->
			if (value instanceof String) queryJson.put(key, new String(value.getBytes(), StandardCharsets.UTF_8));
		}
		
		HTTPBuilder httpBuilder = new HTTPBuilder();
		httpBuilder.request(this.serviceURL, GET, JSON) { req ->
			uri.path = pathInput;
			uri.query = queryJson;
			requestContentType = JSON;
			headers.Accept = 'application/json;charset=utf-8';
			response.success = { resp, json ->
				return json.id[0];
			}
		}
	}

	public String post(String pathInput, LinkedHashMap jsonBody){
		HTTPBuilder httpBuilder = new HTTPBuilder();
		httpBuilder.request(this.serviceURL, POST, JSON) { req ->
			uri.path = pathInput;
			body = jsonBody;
			response.success = { resp, json ->
				return json.id;
			}
		}
	}

	public String postWithHeaderInfo(String pathInput, LinkedHashMap jsonBody, String contentLanguage){
		HTTPBuilder httpBuilder = new HTTPBuilder();
		httpBuilder.request(this.serviceURL, POST, JSON) { req ->
			uri.path = pathInput;
			body = jsonBody;
			headers.'Content-Language' = contentLanguage;
			response.success = { resp, json ->
				return json.id;
			}
		}
	}

	public String put(String pathInput, LinkedHashMap jsonBody){
		HTTPBuilder httpBuilder = new HTTPBuilder();
		httpBuilder.request(this.serviceURL, PUT, JSON) { req ->
			uri.path = pathInput;
			body = jsonBody;
			response.success = { resp, json ->
				return json.id;
			}
		}
	}

	public String put(String pathInput, List jsonBody){
		HTTPBuilder httpBuilder = new HTTPBuilder();
		httpBuilder.request(this.serviceURL, PUT, JSON) { req ->
			uri.path = pathInput;
			body = jsonBody;
			response.success = { resp, json ->
				return json.id;
			}
		}
	}

	public LinkedHashMap convertFromReadableArrayListToJson(def sitData){
		Yaml yaml = new Yaml()
		def obj = yaml.load(sitData);
		return obj
	}

	public String modifyDateFormat(String inputDate) {
		String valueAfterChangeFormat = null;
		if (inputDate != null && inputDate != "") {
			LocalDate changedDate = LocalDate.parse(inputDate, DateTimeFormatter.ofPattern(DATE_FORMAT_USING_IN_GROOVY));
			valueAfterChangeFormat = changedDate.format(DateTimeFormatter.ofPattern(DATE_TIME_SHORT_FORMAT_CONFORM_TO_ISO_8601));
			valueAfterChangeFormat += "T00:00:00.000Z";
		}
		return valueAfterChangeFormat;
	}

	public String extractURL(String hostnameParam, String portParam, String serviceContextPathParam) {
		return "http://$hostnameParam:$portParam/$serviceContextPathParam/";
	}

	public String getServiceContextPath() {
		return this.serviceContextPath;
	}
}
