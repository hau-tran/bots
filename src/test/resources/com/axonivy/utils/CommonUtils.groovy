package com.axonivy.utils

import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.*
import static groovyx.net.http.ContentType.*
import groovy.json.*
import org.yaml.snakeyaml.Yaml
import java.time.LocalDate
import java.time.format.DateTimeFormatter

public class CommonUtils{
  public static final String DATE_FORMAT_USING_IN_GROOVY = "dd.MM.yyyy"
  public static final String DATE_TIME_SHORT_FORMAT_CONFORM_TO_ISO_8601 = "yyyy-MM-dd"

  public String postCommon(String hostInput, String pathInput, LinkedHashMap jsonBody){
      HTTPBuilder httpBuilder = new HTTPBuilder()
      httpBuilder.request(hostInput, POST, JSON) { req ->
        uri.path = pathInput
        body = jsonBody

        println "uri path: " + uri.path
        println "body: " + body
        response.success = { resp, json ->
              return json.id
          }
      }
  }

  public String putCommon(String hostInput, String pathInput, LinkedHashMap jsonBody){
      HTTPBuilder httpBuilder = new HTTPBuilder()
      httpBuilder.request(hostInput, PUT, JSON) { req ->
        uri.path = pathInput
        body = jsonBody
        response.success = { resp, json ->
              return json.id
          }
      }
  }

  public String putCommon(String hostInput, String pathInput, List jsonBody){
    HTTPBuilder httpBuilder = new HTTPBuilder()
    httpBuilder.request(hostInput, PUT, JSON) { req ->
      uri.path = pathInput
      body = jsonBody
      response.success = { resp, json ->
            return json.id
        }
    }
  }

  public LinkedHashMap convertFromReadableArrayListToJson(def sitData){
    Yaml yaml = new Yaml()
    def obj = yaml.load(sitData);
    return obj
  }

  public String modifyDateFormat(String inputDate) {
    String valueAfterChangeFormat = null
    if (inputDate != null && inputDate != "") {
      LocalDate changedDate = LocalDate.parse(inputDate, DateTimeFormatter.ofPattern(DATE_FORMAT_USING_IN_GROOVY));
      valueAfterChangeFormat = changedDate.format(DateTimeFormatter.ofPattern(DATE_TIME_SHORT_FORMAT_CONFORM_TO_ISO_8601));
      valueAfterChangeFormat += "T00:00:00.000Z";
    }
    return valueAfterChangeFormat
  }

}