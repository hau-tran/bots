package com.axonivy.utils

// In this class, just introduce the path for calling the rest resources.
// Not the URI for putting to json data
public class Constants {

    public static final String LOCAL_PERSON_HOST = "http://localhost:8080/luz_person/"
    public static final String LOCAL_COMPENSATION_HOST = "http://localhost:8080/luz_compensation/"
    public static final String API_CONTEXT = "api"
    public static final String SLASH = "/"

    // for compensation
    public static final String LUZ_COMPENSATION_CONTEXT = "/luz_compensation"
    
    public static final String GLOBAL_CONTEXT = "/global-insurers"
    public static final String GLOBAL_INSURER_PATH = API_CONTEXT + GLOBAL_CONTEXT

    public static final String COUNTRY_CONTEXT = "/countries"
    public static final String COUNTRY_PATH = API_CONTEXT + COUNTRY_CONTEXT

    public static final String STATE_CONTEXT = "/states"
    public static final String STATE_PATH = API_CONTEXT + STATE_CONTEXT

    public static final String CITY_CONTEXT = "/cities"
    public static final String CITY_PATH = API_CONTEXT + CITY_CONTEXT
    
    public static final String COMPANY_CONTEXT = "/companies"
    public static final String COMPANY_PATH = API_CONTEXT + COMPANY_CONTEXT

    public static final String WORKPLACE_CONTEXT = "/workplaces"

    public static final String CONTRACT_CONTEXT = "/contracts"
    public static final String CONTRACT_VARIABLE_CONTEXT = "/variables"
    public static final String PAYSLIP_CONTEXT = "/payslips"
    public static final String EMPLOYEE_CONTEXT = "/employees"
    public static final String FAK_BENEFICIARY_CONTEXT = "/fak-beneficiary-for"

    public static final String SIT_CONFIGURATION_CONTEXT = "/salary-configurations"
    public static final String SIT_CONFIGURATION_PATH = API_CONTEXT + SIT_CONFIGURATION_CONTEXT

    public static final String SIT_GROUP_CONTEXT = "/salary-item-type-groups"
    public static final String SIT_GROUP_PATH = API_CONTEXT + SIT_GROUP_CONTEXT

    public static final String SIT_CONTEXT = "/salary-item-types"
    public static final String SIT_PATH = API_CONTEXT + SIT_CONTEXT
    public static final String SIT_VARIABLE_CONTEXT = "/variables"

    public static final String GLOBAL_VARIABLE_CONTEXT = "/variables"
    public static final String GLOBAL_VARIABLE_PATH = API_CONTEXT + GLOBAL_VARIABLE_CONTEXT

    public static String getContractPath(String companyId) {
      return COMPANY_PATH + "/$companyId" + CONTRACT_CONTEXT
    }

    public static String getContractVariablePath(String companyId, String contractId) {
        return COMPANY_PATH + "/$companyId" + CONTRACT_CONTEXT + "/$contractId" + CONTRACT_VARIABLE_CONTEXT
    }

    public static String getPayslipPath(String companyId, String contractId) {
      return COMPANY_PATH + "/$companyId" + CONTRACT_CONTEXT + "/$contractId" + PAYSLIP_CONTEXT
    }

    public static String getEmployeePath(String companyId) {
        return COMPANY_PATH + "/$companyId" + EMPLOYEE_CONTEXT
    }

    public static String getWorkplacePath(String companyId) {
        return COMPANY_PATH + "/$companyId" + WORKPLACE_CONTEXT
    }

    public static String getSalaryItemTypeVariablePath(String salaryItemTypeId) {
        return SIT_PATH + "/$salaryItemTypeId" + SIT_VARIABLE_CONTEXT
    }

    // for person
    public static final String LUZ_PERSON_CONTEXT = "/luz_person"

    public static final String COMPANY_PERSON_CONTEXT = '/companies'
    public static final String COMPANY_PERSON_PATH = API_CONTEXT + COMPANY_PERSON_CONTEXT

    public static final String PERSON_CONTEXT = '/persons'
    public static final String PERSON_PATH = API_CONTEXT + PERSON_CONTEXT
}
