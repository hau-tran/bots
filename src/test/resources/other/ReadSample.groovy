import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes


Path sample = Paths.get(resources, 'samplefile.txt');

println sample.toAbsolutePath()
println Files.readAllLines(sample)