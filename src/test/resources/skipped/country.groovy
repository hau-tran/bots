import groovyx.net.http.RESTClient
import static com.axonivy.utils.Constants.*

String personHost = '{personHost}' 

if (personHost.startsWith('{')) {
    personHost = LOCAL_PERSON_HOST
}

println LOCAL_PERSON_HOST

def luzPersonClient = new RESTClient( personHost )

def country1 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Afghanistan",
"iso2Code":"AF",
"iso3Code":"AFG",
"numericCode":"004",
"phoneCode":"0093"
],
requestContentType : 'application/json' )

def country2 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"ALA Aland Islands",
"iso2Code":"AX",
"iso3Code":"ALA",
"numericCode":"248"
],
requestContentType : 'application/json' )

def country3 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Albania",
"iso2Code":"AL",
"iso3Code":"ALB",
"numericCode":"008",
"phoneCode":"0355"
],
requestContentType : 'application/json' )

def country4 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Algeria",
"iso2Code":"DZ",
"iso3Code":"DZA",
"numericCode":"012",
"phoneCode":"0213"
],
requestContentType : 'application/json' )

def country5 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"American Samoa",
"iso2Code":"AS",
"iso3Code":"ASM",
"numericCode":"016",
"phoneCode":"1684"
],
requestContentType : 'application/json' )

def country6 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Andorra",
"iso2Code":"AD",
"iso3Code":"AND",
"numericCode":"020",
"phoneCode":"0376"
],
requestContentType : 'application/json' )

def country7 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Angola",
"iso2Code":"AO",
"iso3Code":"AGO",
"numericCode":"024",
"phoneCode":"0244"
],
requestContentType : 'application/json' )

def country8 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Anguilla",
"iso2Code":"AI",
"iso3Code":"AIA",
"numericCode":"660",
"phoneCode":"1264"
],
requestContentType : 'application/json' )

def country9 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Antarctica",
"iso2Code":"AQ",
"iso3Code":"ATA",
"numericCode":"010"
],
requestContentType : 'application/json' )

def country10 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Antigua and Barbuda",
"iso2Code":"AG",
"iso3Code":"ATG",
"numericCode":"028",
"phoneCode":"1268"
],
requestContentType : 'application/json' )

def country11 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Argentina",
"iso2Code":"AR",
"iso3Code":"ARG",
"numericCode":"032",
"phoneCode":"0054"
],
requestContentType : 'application/json' )

def country12 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Armenia",
"iso2Code":"AM",
"iso3Code":"ARM",
"numericCode":"051",
"phoneCode":"0374"
],
requestContentType : 'application/json' )

def country13 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Aruba",
"iso2Code":"AW",
"iso3Code":"ABW",
"numericCode":"533",
"phoneCode":"0297"
],
requestContentType : 'application/json' )

def country14 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Australia",
"iso2Code":"AU",
"iso3Code":"AUS",
"numericCode":"036",
"phoneCode":"0061"
],
requestContentType : 'application/json' )

def country15 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Austria",
"iso2Code":"AT",
"iso3Code":"AUT",
"numericCode":"040",
"phoneCode":"0043"
],
requestContentType : 'application/json' )

def country16 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Azerbaijan",
"iso2Code":"AZ",
"iso3Code":"AZE",
"numericCode":"031",
"phoneCode":"0994"
],
requestContentType : 'application/json' )

def country17 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bahamas",
"iso2Code":"BS",
"iso3Code":"BHS",
"numericCode":"044",
"phoneCode":"1242"
],
requestContentType : 'application/json' )

def country18 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bahrain",
"iso2Code":"BH",
"iso3Code":"BHR",
"numericCode":"048",
"phoneCode":"0973"
],
requestContentType : 'application/json' )

def country19 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bangladesh",
"iso2Code":"BD",
"iso3Code":"BGD",
"numericCode":"050",
"phoneCode":"0880"
],
requestContentType : 'application/json' )

def country20 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Barbados",
"iso2Code":"BB",
"iso3Code":"BRB",
"numericCode":"052",
"phoneCode":"1264"
],
requestContentType : 'application/json' )

def country21 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Belarus",
"iso2Code":"BY",
"iso3Code":"BLR",
"numericCode":"112",
"phoneCode":"0375"
],
requestContentType : 'application/json' )

def country22 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Belgium",
"iso2Code":"BE",
"iso3Code":"BEL",
"numericCode":"056",
"phoneCode":"0032"
],
requestContentType : 'application/json' )

def country23 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Belize",
"iso2Code":"BZ",
"iso3Code":"BLZ",
"numericCode":"084",
"phoneCode":"0501"
],
requestContentType : 'application/json' )

def country24 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Benin",
"iso2Code":"BJ",
"iso3Code":"BEN",
"numericCode":"204",
"phoneCode":"0229"
],
requestContentType : 'application/json' )

def country25 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bermuda",
"iso2Code":"BM",
"iso3Code":"BMU",
"numericCode":"060",
"phoneCode":"1441"
],
requestContentType : 'application/json' )

def country26 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bhutan",
"iso2Code":"BT",
"iso3Code":"BTN",
"numericCode":"064",
"phoneCode":"0975"
],
requestContentType : 'application/json' )

def country27 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bolivia",
"iso2Code":"BO",
"iso3Code":"BOL",
"numericCode":"068",
"phoneCode":"0591"
],
requestContentType : 'application/json' )

def country28 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bosnia and Herzegovina",
"iso2Code":"BA",
"iso3Code":"BIH",
"numericCode":"070",
"phoneCode":"0387"
],
requestContentType : 'application/json' )

def country29 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Botswana",
"iso2Code":"BW",
"iso3Code":"BWA",
"numericCode":"072",
"phoneCode":"0267"
],
requestContentType : 'application/json' )

def country30 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bouvet Island",
"iso2Code":"BV",
"iso3Code":"BVT",
"numericCode":"074"
],
requestContentType : 'application/json' )

def country31 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Brazil",
"iso2Code":"BR",
"iso3Code":"BRA",
"numericCode":"076",
"phoneCode":"0055"
],
requestContentType : 'application/json' )

def country32 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"British Virgin Islands",
"iso2Code":"VG",
"iso3Code":"VGB",
"numericCode":"092"
],
requestContentType : 'application/json' )

def country33 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"British Indian Ocean Territory",
"iso2Code":"IO",
"iso3Code":"IOT",
"numericCode":"086"
],
requestContentType : 'application/json' )

def country34 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Brunei Darussalam",
"iso2Code":"BN",
"iso3Code":"BRN",
"numericCode":"096",
"phoneCode":"0673"
],
requestContentType : 'application/json' )

def country35 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Bulgaria",
"iso2Code":"BG",
"iso3Code":"BGR",
"numericCode":"100",
"phoneCode":"0359"
],
requestContentType : 'application/json' )

def country36 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Burkina Faso",
"iso2Code":"BF",
"iso3Code":"BFA",
"numericCode":"854",
"phoneCode":"0226"
],
requestContentType : 'application/json' )

def country37 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Burundi",
"iso2Code":"BI",
"iso3Code":"BDI",
"numericCode":"108",
"phoneCode":"0257"
],
requestContentType : 'application/json' )

def country38 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cambodia",
"iso2Code":"KH",
"iso3Code":"KHM",
"numericCode":"116",
"phoneCode":"0855"
],
requestContentType : 'application/json' )

def country39 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cameroon",
"iso2Code":"CM",
"iso3Code":"CMR",
"numericCode":"120",
"phoneCode":"0237"
],
requestContentType : 'application/json' )

def country40 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Canada",
"iso2Code":"CA",
"iso3Code":"CAN",
"numericCode":"124",
"phoneCode":"0001"
],
requestContentType : 'application/json' )

def country41 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cape Verde",
"iso2Code":"CV",
"iso3Code":"CPV",
"numericCode":"132",
"phoneCode":"0238"
],
requestContentType : 'application/json' )

def country42 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cayman Islands",
"iso2Code":"KY",
"iso3Code":"CYM",
"numericCode":"136",
"phoneCode":"1345"
],
requestContentType : 'application/json' )

def country43 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Central African Republic",
"iso2Code":"CF",
"iso3Code":"CAF",
"numericCode":"140",
"phoneCode":"0236"
],
requestContentType : 'application/json' )

def country44 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Chad",
"iso2Code":"TD",
"iso3Code":"TCD",
"numericCode":"148",
"phoneCode":"0235"
],
requestContentType : 'application/json' )

def country45 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Chile",
"iso2Code":"CL",
"iso3Code":"CHL",
"numericCode":"152",
"phoneCode":"0056"
],
requestContentType : 'application/json' )

def country46 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"China",
"iso2Code":"CN",
"iso3Code":"CHN",
"numericCode":"156",
"phoneCode":"0086"
],
requestContentType : 'application/json' )

def country47 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Hong Kong",
"iso2Code":"HK",
"iso3Code":"HKG",
"numericCode":"344",
"phoneCode":"0852"
],
requestContentType : 'application/json' )

def country48 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Macao",
"iso2Code":"MO",
"iso3Code":"MAC",
"numericCode":"446",
"phoneCode":"0853"
],
requestContentType : 'application/json' )

def country49 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Christmas Island",
"iso2Code":"CX",
"iso3Code":"CXR",
"numericCode":"162",
"phoneCode":"0061"
],
requestContentType : 'application/json' )

def country50 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cocos (Keeling) Islands",
"iso2Code":"CC",
"iso3Code":"CCK",
"numericCode":"166",
"phoneCode":"0061"
],
requestContentType : 'application/json' )

def country51 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Colombia",
"iso2Code":"CO",
"iso3Code":"COL",
"numericCode":"170",
"phoneCode":"0057"
],
requestContentType : 'application/json' )

def country52 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Comoros",
"iso2Code":"KM",
"iso3Code":"COM",
"numericCode":"174",
"phoneCode":"0269"
],
requestContentType : 'application/json' )

def country53 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Congo (Brazzaville)",
"iso2Code":"CG",
"iso3Code":"COG",
"numericCode":"178",
"phoneCode":"0242"
],
requestContentType : 'application/json' )

def country54 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Congo",
"iso2Code":"CD",
"iso3Code":"COD",
"numericCode":"180",
"phoneCode":"0243"
],
requestContentType : 'application/json' )

def country55 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cook Islands",
"iso2Code":"CK",
"iso3Code":"COK",
"numericCode":"184",
"phoneCode":"0682"
],
requestContentType : 'application/json' )

def country56 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Costa Rica",
"iso2Code":"CR",
"iso3Code":"CRI",
"numericCode":"188",
"phoneCode":"0506"
],
requestContentType : 'application/json' )

def country57 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Côte d'Ivoire",
"iso2Code":"CI",
"iso3Code":"CIV",
"numericCode":"384",
"phoneCode":"0225"
],
requestContentType : 'application/json' )

def country58 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Croatia",
"iso2Code":"HR",
"iso3Code":"HRV",
"numericCode":"191",
"phoneCode":"0385"
],
requestContentType : 'application/json' )

def country59 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cuba",
"iso2Code":"CU",
"iso3Code":"CUB",
"numericCode":"192",
"phoneCode":"0053"
],
requestContentType : 'application/json' )

def country60 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Cyprus",
"iso2Code":"CY",
"iso3Code":"CYP",
"numericCode":"196",
"phoneCode":"0357"
],
requestContentType : 'application/json' )

def country61 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Czech Republic",
"iso2Code":"CZ",
"iso3Code":"CZE",
"numericCode":"203",
"phoneCode":"0420"
],
requestContentType : 'application/json' )

def country62 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Denmark",
"iso2Code":"DK",
"iso3Code":"DNK",
"numericCode":"208",
"phoneCode":"0045"
],
requestContentType : 'application/json' )

def country63 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Djibouti",
"iso2Code":"DJ",
"iso3Code":"DJI",
"numericCode":"262",
"phoneCode":"0253"
],
requestContentType : 'application/json' )

def country64 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Dominica",
"iso2Code":"DM",
"iso3Code":"DMA",
"numericCode":"212",
"phoneCode":"1767"
],
requestContentType : 'application/json' )

def country65 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Dominican Republic",
"iso2Code":"DO",
"iso3Code":"DOM",
"numericCode":"214",
"phoneCode":"1809"
],
requestContentType : 'application/json' )

def country66 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Ecuador",
"iso2Code":"EC",
"iso3Code":"ECU",
"numericCode":"218",
"phoneCode":"0593"
],
requestContentType : 'application/json' )

def country67 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Egypt",
"iso2Code":"EG",
"iso3Code":"EGY",
"numericCode":"818",
"phoneCode":"0020"
],
requestContentType : 'application/json' )

def country68 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"El Salvador",
"iso2Code":"SV",
"iso3Code":"SLV",
"numericCode":"222",
"phoneCode":"0503"
],
requestContentType : 'application/json' )

def country69 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Equatorial Guinea",
"iso2Code":"GQ",
"iso3Code":"GNQ",
"numericCode":"226",
"phoneCode":"0240"
],
requestContentType : 'application/json' )

def country70 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Eritrea",
"iso2Code":"ER",
"iso3Code":"ERI",
"numericCode":"232",
"phoneCode":"0291"
],
requestContentType : 'application/json' )

def country71 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Estonia",
"iso2Code":"EE",
"iso3Code":"EST",
"numericCode":"233",
"phoneCode":"0372"
],
requestContentType : 'application/json' )

def country72 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Ethiopia",
"iso2Code":"ET",
"iso3Code":"ETH",
"numericCode":"231",
"phoneCode":"0251"
],
requestContentType : 'application/json' )

def country73 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Falkland Islands (Malvinas)",
"iso2Code":"FK",
"iso3Code":"FLK",
"numericCode":"238",
"phoneCode":"0500"
],
requestContentType : 'application/json' )

def country74 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Faroe Islands",
"iso2Code":"FO",
"iso3Code":"FRO",
"numericCode":"234",
"phoneCode":"0298"
],
requestContentType : 'application/json' )

def country75 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Fiji",
"iso2Code":"FJ",
"iso3Code":"FJI",
"numericCode":"242",
"phoneCode":"0679"
],
requestContentType : 'application/json' )

def country76 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Finland",
"iso2Code":"FI",
"iso3Code":"FIN",
"numericCode":"246",
"phoneCode":"0358"
],
requestContentType : 'application/json' )

def country77 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"France",
"iso2Code":"FR",
"iso3Code":"FRA",
"numericCode":"250",
"phoneCode":"0033"
],
requestContentType : 'application/json' )

def country78 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"French Guiana",
"iso2Code":"GF",
"iso3Code":"GUF",
"numericCode":"254",
"phoneCode":"0594"
],
requestContentType : 'application/json' )

def country79 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"French Polynesia",
"iso2Code":"PF",
"iso3Code":"PYF",
"numericCode":"258",
"phoneCode":"0689"
],
requestContentType : 'application/json' )

def country80 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"French Southern Territories",
"iso2Code":"TF",
"iso3Code":"ATF",
"numericCode":"260"
],
requestContentType : 'application/json' )

def country81 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Gabon",
"iso2Code":"GA",
"iso3Code":"GAB",
"numericCode":"266",
"phoneCode":"0241"
],
requestContentType : 'application/json' )

def country82 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Gambia",
"iso2Code":"GM",
"iso3Code":"GMB",
"numericCode":"270",
"phoneCode":"0220"
],
requestContentType : 'application/json' )

def country83 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Georgia",
"iso2Code":"GE",
"iso3Code":"GEO",
"numericCode":"268",
"phoneCode":"0995"
],
requestContentType : 'application/json' )

def country84 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Germany",
"iso2Code":"DE",
"iso3Code":"DEU",
"numericCode":"276",
"phoneCode":"0049"
],
requestContentType : 'application/json' )

def country85 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Ghana",
"iso2Code":"GH",
"iso3Code":"GHA",
"numericCode":"288",
"phoneCode":"0233"
],
requestContentType : 'application/json' )

def country86 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Gibraltar",
"iso2Code":"GI",
"iso3Code":"GIB",
"numericCode":"292",
"phoneCode":"0350"
],
requestContentType : 'application/json' )

def country87 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Greece",
"iso2Code":"GR",
"iso3Code":"GRC",
"numericCode":"300",
"phoneCode":"0030"
],
requestContentType : 'application/json' )

def country88 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Greenland",
"iso2Code":"GL",
"iso3Code":"GRL",
"numericCode":"304",
"phoneCode":"0299"
],
requestContentType : 'application/json' )

def country89 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Grenada",
"iso2Code":"GD",
"iso3Code":"GRD",
"numericCode":"308",
"phoneCode":"1473"
],
requestContentType : 'application/json' )

def country90 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Guadeloupe",
"iso2Code":"GP",
"iso3Code":"GLP",
"numericCode":"312",
"phoneCode":"0590"
],
requestContentType : 'application/json' )

def country91 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Guam",
"iso2Code":"GU",
"iso3Code":"GUM",
"numericCode":"316",
"phoneCode":"1671"
],
requestContentType : 'application/json' )

def country92 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Guatemala",
"iso2Code":"GT",
"iso3Code":"GTM",
"numericCode":"320",
"phoneCode":"0502"
],
requestContentType : 'application/json' )

def country93 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Guernsey",
"iso2Code":"GG",
"iso3Code":"GGY",
"numericCode":"831",
"phoneCode":"0224"
],
requestContentType : 'application/json' )

def country94 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Guinea",
"iso2Code":"GN",
"iso3Code":"GIN",
"numericCode":"324",
"phoneCode":"0245"
],
requestContentType : 'application/json' )

def country95 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Guinea-Bissau",
"iso2Code":"GW",
"iso3Code":"GNB",
"numericCode":"624"
],
requestContentType : 'application/json' )

def country96 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Guyana",
"iso2Code":"GY",
"iso3Code":"GUY",
"numericCode":"328",
"phoneCode":"0592"
],
requestContentType : 'application/json' )

def country97 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Haiti",
"iso2Code":"HT",
"iso3Code":"HTI",
"numericCode":"332",
"phoneCode":"0509"
],
requestContentType : 'application/json' )

def country98 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Heard Island and Mcdonald Islands",
"iso2Code":"HM",
"iso3Code":"HMD",
"numericCode":"334"
],
requestContentType : 'application/json' )

def country99 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Holy See (Vatican City State)",
"iso2Code":"VA",
"iso3Code":"VAT",
"numericCode":"336",
"phoneCode":"0379"
],
requestContentType : 'application/json' )

def country100 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Honduras",
"iso2Code":"HN",
"iso3Code":"HND",
"numericCode":"340",
"phoneCode":"0504"
],
requestContentType : 'application/json' )

def country101 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Hungary",
"iso2Code":"HU",
"iso3Code":"HUN",
"numericCode":"348",
"phoneCode":"0036"
],
requestContentType : 'application/json' )

def country102 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Iceland",
"iso2Code":"IS",
"iso3Code":"ISL",
"numericCode":"352",
"phoneCode":"0354"
],
requestContentType : 'application/json' )

def country103 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"India",
"iso2Code":"IN",
"iso3Code":"IND",
"numericCode":"356",
"phoneCode":"0091"
],
requestContentType : 'application/json' )

def country104 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Indonesia",
"iso2Code":"ID",
"iso3Code":"IDN",
"numericCode":"360",
"phoneCode":"0062"
],
requestContentType : 'application/json' )

def country105 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Iran",
"iso2Code":"IR",
"iso3Code":"IRN",
"numericCode":"364",
"phoneCode":"0098"
],
requestContentType : 'application/json' )

def country106 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Iraq",
"iso2Code":"IQ",
"iso3Code":"IRQ",
"numericCode":"368",
"phoneCode":"0964"
],
requestContentType : 'application/json' )

def country107 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Ireland",
"iso2Code":"IE",
"iso3Code":"IRL",
"numericCode":"372",
"phoneCode":"0353"
],
requestContentType : 'application/json' )

def country108 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Isle of Man",
"iso2Code":"IM",
"iso3Code":"IMN",
"numericCode":"833"
],
requestContentType : 'application/json' )

def country109 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Israel",
"iso2Code":"IL",
"iso3Code":"ISR",
"numericCode":"376",
"phoneCode":"0972"
],
requestContentType : 'application/json' )

def country110 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Italy",
"iso2Code":"IT",
"iso3Code":"ITA",
"numericCode":"380",
"phoneCode":"0039"
],
requestContentType : 'application/json' )

def country111 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Jamaica",
"iso2Code":"JM",
"iso3Code":"JAM",
"numericCode":"388",
"phoneCode":"1876"
],
requestContentType : 'application/json' )

def country112 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Japan",
"iso2Code":"JP",
"iso3Code":"JPN",
"numericCode":"392",
"phoneCode":"0081"
],
requestContentType : 'application/json' )

def country113 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Jersey",
"iso2Code":"JE",
"iso3Code":"JEY",
"numericCode":"832"
],
requestContentType : 'application/json' )

def country114 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Jordan",
"iso2Code":"JO",
"iso3Code":"JOR",
"numericCode":"400",
"phoneCode":"0962"
],
requestContentType : 'application/json' )

def country115 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Kazakhstan",
"iso2Code":"KZ",
"iso3Code":"KAZ",
"numericCode":"398",
"phoneCode":"0007"
],
requestContentType : 'application/json' )

def country116 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Kenya",
"iso2Code":"KE",
"iso3Code":"KEN",
"numericCode":"404",
"phoneCode":"0254"
],
requestContentType : 'application/json' )

def country117 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Kiribati",
"iso2Code":"KI",
"iso3Code":"KIR",
"numericCode":"296",
"phoneCode":"0686"
],
requestContentType : 'application/json' )

def country118 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Democratic People's Republic of Korea",
"iso2Code":"KP",
"iso3Code":"PRK",
"numericCode":"408",
"phoneCode":"0850"
],
requestContentType : 'application/json' )

def country119 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Korea",
"iso2Code":"KR",
"iso3Code":"KOR",
"numericCode":"410",
"phoneCode":"0082"
],
requestContentType : 'application/json' )

def country120 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Kuwait",
"iso2Code":"KW",
"iso3Code":"KWT",
"numericCode":"414",
"phoneCode":"0965"
],
requestContentType : 'application/json' )

def country121 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Kyrgyzstan",
"iso2Code":"KG",
"iso3Code":"KGZ",
"numericCode":"417",
"phoneCode":"0996"
],
requestContentType : 'application/json' )

def country122 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Laos",
"iso2Code":"LA",
"iso3Code":"LAO",
"numericCode":"418",
"phoneCode":"0856"
],
requestContentType : 'application/json' )

def country123 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Latvia",
"iso2Code":"LV",
"iso3Code":"LVA",
"numericCode":"428",
"phoneCode":"0371"
],
requestContentType : 'application/json' )

def country124 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Lebanon",
"iso2Code":"LB",
"iso3Code":"LBN",
"numericCode":"422",
"phoneCode":"0961"
],
requestContentType : 'application/json' )

def country125 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Lesotho",
"iso2Code":"LS",
"iso3Code":"LSO",
"numericCode":"426",
"phoneCode":"0266"
],
requestContentType : 'application/json' )

def country126 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Liberia",
"iso2Code":"LR",
"iso3Code":"LBR",
"numericCode":"430",
"phoneCode":"0231"
],
requestContentType : 'application/json' )

def country127 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Libya",
"iso2Code":"LY",
"iso3Code":"LBY",
"numericCode":"434",
"phoneCode":"0218"
],
requestContentType : 'application/json' )

def country128 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Liechtenstein",
"iso2Code":"LI",
"iso3Code":"LIE",
"numericCode":"438",
"phoneCode":"0423"
],
requestContentType : 'application/json' )

def country129 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Lithuania",
"iso2Code":"LT",
"iso3Code":"LTU",
"numericCode":"440",
"phoneCode":"0370"
],
requestContentType : 'application/json' )

def country130 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Luxembourg",
"iso2Code":"LU",
"iso3Code":"LUX",
"numericCode":"442",
"phoneCode":"0352"
],
requestContentType : 'application/json' )

def country131 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Macedonia",
"iso2Code":"MK",
"iso3Code":"MKD",
"numericCode":"807",
"phoneCode":"0389"
],
requestContentType : 'application/json' )

def country132 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Madagascar",
"iso2Code":"MG",
"iso3Code":"MDG",
"numericCode":"450",
"phoneCode":"0261"
],
requestContentType : 'application/json' )

def country133 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Malawi",
"iso2Code":"MW",
"iso3Code":"MWI",
"numericCode":"454",
"phoneCode":"0265"
],
requestContentType : 'application/json' )

def country134 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Malaysia",
"iso2Code":"MY",
"iso3Code":"MYS",
"numericCode":"458",
"phoneCode":"0060"
],
requestContentType : 'application/json' )

def country135 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Maldives",
"iso2Code":"MV",
"iso3Code":"MDV",
"numericCode":"462",
"phoneCode":"0960"
],
requestContentType : 'application/json' )

def country136 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Mali",
"iso2Code":"ML",
"iso3Code":"MLI",
"numericCode":"466",
"phoneCode":"0223"
],
requestContentType : 'application/json' )

def country137 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Malta",
"iso2Code":"MT",
"iso3Code":"MLT",
"numericCode":"470",
"phoneCode":"0356"
],
requestContentType : 'application/json' )

def country138 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Marshall Islands",
"iso2Code":"MH",
"iso3Code":"MHL",
"numericCode":"584",
"phoneCode":"0692"
],
requestContentType : 'application/json' )

def country139 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Martinique",
"iso2Code":"MQ",
"iso3Code":"MTQ",
"numericCode":"474",
"phoneCode":"0596"
],
requestContentType : 'application/json' )

def country140 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Mauritania",
"iso2Code":"MR",
"iso3Code":"MRT",
"numericCode":"478",
"phoneCode":"0222"
],
requestContentType : 'application/json' )

def country141 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Mauritius",
"iso2Code":"MU",
"iso3Code":"MUS",
"numericCode":"480",
"phoneCode":"0230"
],
requestContentType : 'application/json' )

def country142 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Mayotte",
"iso2Code":"YT",
"iso3Code":"MYT",
"numericCode":"175",
"phoneCode":"0262"
],
requestContentType : 'application/json' )

def country143 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Mexico",
"iso2Code":"MX",
"iso3Code":"MEX",
"numericCode":"484",
"phoneCode":"0052"
],
requestContentType : 'application/json' )

def country144 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Federated States of Micronesia",
"iso2Code":"FM",
"iso3Code":"FSM",
"numericCode":"583",
"phoneCode":"0691"
],
requestContentType : 'application/json' )

def country145 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Moldova",
"iso2Code":"MD",
"iso3Code":"MDA",
"numericCode":"498",
"phoneCode":"0373"
],
requestContentType : 'application/json' )

def country146 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Monaco",
"iso2Code":"MC",
"iso3Code":"MCO",
"numericCode":"492",
"phoneCode":"0377"
],
requestContentType : 'application/json' )

def country147 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Mongolia",
"iso2Code":"MN",
"iso3Code":"MNG",
"numericCode":"496",
"phoneCode":"0976"
],
requestContentType : 'application/json' )

def country148 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Montenegro",
"iso2Code":"ME",
"iso3Code":"MNE",
"numericCode":"499",
"phoneCode":"0382"
],
requestContentType : 'application/json' )

def country149 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Montserrat",
"iso2Code":"MS",
"iso3Code":"MSR",
"numericCode":"500",
"phoneCode":"1664"
],
requestContentType : 'application/json' )

def country150 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Morocco",
"iso2Code":"MA",
"iso3Code":"MAR",
"numericCode":"504",
"phoneCode":"0212"
],
requestContentType : 'application/json' )

def country151 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Mozambique",
"iso2Code":"MZ",
"iso3Code":"MOZ",
"numericCode":"508",
"phoneCode":"0258"
],
requestContentType : 'application/json' )

def country152 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Myanmar",
"iso2Code":"MM",
"iso3Code":"MMR",
"numericCode":"104",
"phoneCode":"0095"
],
requestContentType : 'application/json' )

def country153 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Namibia",
"iso2Code":"NA",
"iso3Code":"NAM",
"numericCode":"516",
"phoneCode":"0264"
],
requestContentType : 'application/json' )

def country154 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Nauru",
"iso2Code":"NR",
"iso3Code":"NRU",
"numericCode":"520",
"phoneCode":"0674"
],
requestContentType : 'application/json' )

def country155 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Nepal",
"iso2Code":"NP",
"iso3Code":"NPL",
"numericCode":"524",
"phoneCode":"0977"
],
requestContentType : 'application/json' )

def country156 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Netherlands",
"iso2Code":"NL",
"iso3Code":"NLD",
"numericCode":"528",
"phoneCode":"0031"
],
requestContentType : 'application/json' )

def country157 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Netherlands Antilles",
"iso2Code":"AN",
"iso3Code":"ANT",
"numericCode":"530",
"phoneCode":"0599"
],
requestContentType : 'application/json' )

def country158 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"New Caledonia",
"iso2Code":"NC",
"iso3Code":"NCL",
"numericCode":"540",
"phoneCode":"0687"
],
requestContentType : 'application/json' )

def country159 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"New Zealand",
"iso2Code":"NZ",
"iso3Code":"NZL",
"numericCode":"554",
"phoneCode":"0064"
],
requestContentType : 'application/json' )

def country160 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Nicaragua",
"iso2Code":"NI",
"iso3Code":"NIC",
"numericCode":"558",
"phoneCode":"0505"
],
requestContentType : 'application/json' )

def country161 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Niger",
"iso2Code":"NE",
"iso3Code":"NER",
"numericCode":"562",
"phoneCode":"0227"
],
requestContentType : 'application/json' )

def country162 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Nigeria",
"iso2Code":"NG",
"iso3Code":"NGA",
"numericCode":"566",
"phoneCode":"0234"
],
requestContentType : 'application/json' )

def country163 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Niue",
"iso2Code":"NU",
"iso3Code":"NIU",
"numericCode":"570",
"phoneCode":"0683"
],
requestContentType : 'application/json' )

def country164 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Norfolk Island",
"iso2Code":"NF",
"iso3Code":"NFK",
"numericCode":"574",
"phoneCode":"0672"
],
requestContentType : 'application/json' )

def country165 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Northern Mariana Islands",
"iso2Code":"MP",
"iso3Code":"MNP",
"numericCode":"580",
"phoneCode":"1670"
],
requestContentType : 'application/json' )

def country166 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Norway",
"iso2Code":"NO",
"iso3Code":"NOR",
"numericCode":"578",
"phoneCode":"0047"
],
requestContentType : 'application/json' )

def country167 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Oman",
"iso2Code":"OM",
"iso3Code":"OMN",
"numericCode":"512",
"phoneCode":"0968"
],
requestContentType : 'application/json' )

def country168 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Pakistan",
"iso2Code":"PK",
"iso3Code":"PAK",
"numericCode":"586",
"phoneCode":"0092"
],
requestContentType : 'application/json' )

def country169 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Palau",
"iso2Code":"PW",
"iso3Code":"PLW",
"numericCode":"585",
"phoneCode":"0680"
],
requestContentType : 'application/json' )

def country170 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Palestinian Territory",
"iso2Code":"PS",
"iso3Code":"PSE",
"numericCode":"275",
"phoneCode":"0970"
],
requestContentType : 'application/json' )

def country171 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Panama",
"iso2Code":"PA",
"iso3Code":"PAN",
"numericCode":"591",
"phoneCode":"0507"
],
requestContentType : 'application/json' )

def country172 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Papua New Guinea",
"iso2Code":"PG",
"iso3Code":"PNG",
"numericCode":"598",
"phoneCode":"0675"
],
requestContentType : 'application/json' )

def country173 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Paraguay",
"iso2Code":"PY",
"iso3Code":"PRY",
"numericCode":"600",
"phoneCode":"0595"
],
requestContentType : 'application/json' )

def country174 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Peru",
"iso2Code":"PE",
"iso3Code":"PER",
"numericCode":"604",
"phoneCode":"0051"
],
requestContentType : 'application/json' )

def country175 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Philippines",
"iso2Code":"PH",
"iso3Code":"PHL",
"numericCode":"608",
"phoneCode":"0063"
],
requestContentType : 'application/json' )

def country176 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Pitcairn",
"iso2Code":"PN",
"iso3Code":"PCN",
"numericCode":"612",
"phoneCode":"0870"
],
requestContentType : 'application/json' )

def country177 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Poland",
"iso2Code":"PL",
"iso3Code":"POL",
"numericCode":"616",
"phoneCode":"0048"
],
requestContentType : 'application/json' )

def country178 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Portugal",
"iso2Code":"PT",
"iso3Code":"PRT",
"numericCode":"620",
"phoneCode":"0351"
],
requestContentType : 'application/json' )

def country179 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Puerto Rico",
"iso2Code":"PR",
"iso3Code":"PRI",
"numericCode":"630",
"phoneCode":"1787"
],
requestContentType : 'application/json' )

def country180 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Qatar",
"iso2Code":"QA",
"iso3Code":"QAT",
"numericCode":"634",
"phoneCode":"0974"
],
requestContentType : 'application/json' )

def country181 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Réunion",
"iso2Code":"RE",
"iso3Code":"REU",
"numericCode":"638",
"phoneCode":"0262"
],
requestContentType : 'application/json' )

def country182 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Romania",
"iso2Code":"RO",
"iso3Code":"ROU",
"numericCode":"642",
"phoneCode":"0040"
],
requestContentType : 'application/json' )

def country183 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Russian Federation",
"iso2Code":"RU",
"iso3Code":"RUS",
"numericCode":"643",
"phoneCode":"0007"
],
requestContentType : 'application/json' )

def country184 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Rwanda",
"iso2Code":"RW",
"iso3Code":"RWA",
"numericCode":"646",
"phoneCode":"0250"
],
requestContentType : 'application/json' )

def country185 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saint-Barthélemy",
"iso2Code":"BL",
"iso3Code":"BLM",
"numericCode":"652",
"phoneCode":"0290"
],
requestContentType : 'application/json' )

def country186 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saint Helena",
"iso2Code":"SH",
"iso3Code":"SHN",
"numericCode":"654",
"phoneCode":"1869"
],
requestContentType : 'application/json' )

def country187 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saint Kitts and Nevis",
"iso2Code":"KN",
"iso3Code":"KNA",
"numericCode":"659",
"phoneCode":"1758"
],
requestContentType : 'application/json' )

def country188 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saint Lucia",
"iso2Code":"LC",
"iso3Code":"LCA",
"numericCode":"662",
"phoneCode":"0508"
],
requestContentType : 'application/json' )

def country189 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saint-Martin (French part)",
"iso2Code":"MF",
"iso3Code":"MAF",
"numericCode":"663",
"phoneCode":"0685"
],
requestContentType : 'application/json' )

def country190 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saint Pierre and Miquelon",
"iso2Code":"PM",
"iso3Code":"SPM",
"numericCode":"666",
"phoneCode":"0378"
],
requestContentType : 'application/json' )

def country191 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saint Vincent and Grenadines",
"iso2Code":"VC",
"iso3Code":"VCT",
"numericCode":"670",
"phoneCode":"0239"
],
requestContentType : 'application/json' )

def country192 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Samoa",
"iso2Code":"WS",
"iso3Code":"WSM",
"numericCode":"882",
"phoneCode":"0685"
],
requestContentType : 'application/json' )

def country193 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"San Marino",
"iso2Code":"SM",
"iso3Code":"SMR",
"numericCode":"674",
"phoneCode":"0378"
],
requestContentType : 'application/json' )

def country194 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Sao Tome and Principe",
"iso2Code":"ST",
"iso3Code":"STP",
"numericCode":"678",
"phoneCode":"0239"
],
requestContentType : 'application/json' )

def country195 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Saudi Arabia",
"iso2Code":"SA",
"iso3Code":"SAU",
"numericCode":"682",
"phoneCode":"0966"
],
requestContentType : 'application/json' )

def country196 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Senegal",
"iso2Code":"SN",
"iso3Code":"SEN",
"numericCode":"686",
"phoneCode":"0221"
],
requestContentType : 'application/json' )

def country197 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Serbia",
"iso2Code":"RS",
"iso3Code":"SRB",
"numericCode":"688",
"phoneCode":"0381"
],
requestContentType : 'application/json' )

def country198 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Seychelles",
"iso2Code":"SC",
"iso3Code":"SYC",
"numericCode":"690",
"phoneCode":"0248"
],
requestContentType : 'application/json' )

def country199 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Sierra Leone",
"iso2Code":"SL",
"iso3Code":"SLE",
"numericCode":"694",
"phoneCode":"0232"
],
requestContentType : 'application/json' )

def country200 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Singapore",
"iso2Code":"SG",
"iso3Code":"SGP",
"numericCode":"702",
"phoneCode":"0065"
],
requestContentType : 'application/json' )

def country201 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Slovakia",
"iso2Code":"SK",
"iso3Code":"SVK",
"numericCode":"703",
"phoneCode":"0421"
],
requestContentType : 'application/json' )

def country202 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Slovenia",
"iso2Code":"SI",
"iso3Code":"SVN",
"numericCode":"705",
"phoneCode":"0386"
],
requestContentType : 'application/json' )

def country203 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Solomon Islands",
"iso2Code":"SB",
"iso3Code":"SLB",
"numericCode":"090",
"phoneCode":"0677"
],
requestContentType : 'application/json' )

def country204 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Somalia",
"iso2Code":"SO",
"iso3Code":"SOM",
"numericCode":"706",
"phoneCode":"0252"
],
requestContentType : 'application/json' )

def country205 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"South Africa",
"iso2Code":"ZA",
"iso3Code":"ZAF",
"numericCode":"710",
"phoneCode":"0027"
],
requestContentType : 'application/json' )

def country206 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"South Georgia and the South Sandwich Islands",
"iso2Code":"GS",
"iso3Code":"SGS",
"numericCode":"239"
],
requestContentType : 'application/json' )

def country207 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"South Sudan",
"iso2Code":"SS",
"iso3Code":"SSD",
"numericCode":"728"
],
requestContentType : 'application/json' )

def country208 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Spain",
"iso2Code":"ES",
"iso3Code":"ESP",
"numericCode":"724",
"phoneCode":"0034"
],
requestContentType : 'application/json' )

def country209 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Sri Lanka",
"iso2Code":"LK",
"iso3Code":"LKA",
"numericCode":"144",
"phoneCode":"0094"
],
requestContentType : 'application/json' )

def country210 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Sudan",
"iso2Code":"SD",
"iso3Code":"SDN",
"numericCode":"736",
"phoneCode":"0249"
],
requestContentType : 'application/json' )

def country211 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Suriname",
"iso2Code":"SR",
"iso3Code":"SUR",
"numericCode":"740",
"phoneCode":"0597"
],
requestContentType : 'application/json' )

def country212 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Svalbard and Jan Mayen Islands",
"iso2Code":"SJ",
"iso3Code":"SJM",
"numericCode":"744",
"phoneCode":"0047"
],
requestContentType : 'application/json' )

def country213 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Swaziland",
"iso2Code":"SZ",
"iso3Code":"SWZ",
"numericCode":"748",
"phoneCode":"0268"
],
requestContentType : 'application/json' )

def country214 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Sweden",
"iso2Code":"SE",
"iso3Code":"SWE",
"numericCode":"752",
"phoneCode":"0046"
],
requestContentType : 'application/json' )

def country215 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Schweiz",
"iso2Code":"CH",
"iso3Code":"CHE",
"numericCode":"756",
"phoneCode":"0041"
],
requestContentType : 'application/json' )

def country216 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Syrian Arab Republic (Syria)",
"iso2Code":"SY",
"iso3Code":"SYR",
"numericCode":"760",
"phoneCode":"0963"
],
requestContentType : 'application/json' )

def country217 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Taiwan",
"iso2Code":"TW",
"iso3Code":"TWN",
"numericCode":"158",
"phoneCode":"0886"
],
requestContentType : 'application/json' )

def country218 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Tajikistan",
"iso2Code":"TJ",
"iso3Code":"TJK",
"numericCode":"762",
"phoneCode":"0992"
],
requestContentType : 'application/json' )

def country219 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Tanzania",
"iso2Code":"TZ",
"iso3Code":"TZA",
"numericCode":"834",
"phoneCode":"0256"
],
requestContentType : 'application/json' )

def country220 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Thailand",
"iso2Code":"TH",
"iso3Code":"THA",
"numericCode":"764",
"phoneCode":"0066"
],
requestContentType : 'application/json' )

def country221 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Timor-Leste",
"iso2Code":"TL",
"iso3Code":"TLS",
"numericCode":"626",
"phoneCode":"0670"
],
requestContentType : 'application/json' )

def country222 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Togo",
"iso2Code":"TG",
"iso3Code":"TGO",
"numericCode":"768",
"phoneCode":"0228"
],
requestContentType : 'application/json' )

def country223 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Tokelau",
"iso2Code":"TK",
"iso3Code":"TKL",
"numericCode":"772",
"phoneCode":"0690"
],
requestContentType : 'application/json' )

def country224 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Tonga",
"iso2Code":"TO",
"iso3Code":"TON",
"numericCode":"776",
"phoneCode":"0676"
],
requestContentType : 'application/json' )

def country225 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Trinidad and Tobago",
"iso2Code":"TT",
"iso3Code":"TTO",
"numericCode":"780",
"phoneCode":"1868"
],
requestContentType : 'application/json' )

def country226 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Tunisia",
"iso2Code":"TN",
"iso3Code":"TUN",
"numericCode":"788",
"phoneCode":"0216"
],
requestContentType : 'application/json' )

def country227 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Turkey",
"iso2Code":"TR",
"iso3Code":"TUR",
"numericCode":"792",
"phoneCode":"0090"
],
requestContentType : 'application/json' )

def country228 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Turkmenistan",
"iso2Code":"TM",
"iso3Code":"TKM",
"numericCode":"795",
"phoneCode":"0993"
],
requestContentType : 'application/json' )

def country229 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Turks and Caicos Islands",
"iso2Code":"TC",
"iso3Code":"TCA",
"numericCode":"796",
"phoneCode":"1649"
],
requestContentType : 'application/json' )

def country230 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Tuvalu",
"iso2Code":"TV",
"iso3Code":"TUV",
"numericCode":"798",
"phoneCode":"0688"
],
requestContentType : 'application/json' )

def country231 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Uganda",
"iso2Code":"UG",
"iso3Code":"UGA",
"numericCode":"800",
"phoneCode":"0256"
],
requestContentType : 'application/json' )

def country232 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Ukraine",
"iso2Code":"UA",
"iso3Code":"UKR",
"numericCode":"804",
"phoneCode":"0380"
],
requestContentType : 'application/json' )

def country233 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"United Arab Emirates",
"iso2Code":"AE",
"iso3Code":"ARE",
"numericCode":"784",
"phoneCode":"0971"
],
requestContentType : 'application/json' )

def country234 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"United Kingdom",
"iso2Code":"GB",
"iso3Code":"GBR",
"numericCode":"826",
"phoneCode":"0044"
],
requestContentType : 'application/json' )

def country235 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"United States of America",
"iso2Code":"US",
"iso3Code":"USA",
"numericCode":"840",
"phoneCode":"0001"
],
requestContentType : 'application/json' )

def country236 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"United States Minor Outlying Islands",
"iso2Code":"UM",
"iso3Code":"UMI",
"numericCode":"581"
],
requestContentType : 'application/json' )

def country237 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Uruguay",
"iso2Code":"UY",
"iso3Code":"URY",
"numericCode":"858",
"phoneCode":"0598"
],
requestContentType : 'application/json' )

def country238 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Uzbekistan",
"iso2Code":"UZ",
"iso3Code":"UZB",
"numericCode":"860",
"phoneCode":"0998"
],
requestContentType : 'application/json' )

def country239 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Vanuatu",
"iso2Code":"VU",
"iso3Code":"VUT",
"numericCode":"548",
"phoneCode":"0678"
],
requestContentType : 'application/json' )

def country240 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Venezuela",
"iso2Code":"VE",
"iso3Code":"VEN",
"numericCode":"862",
"phoneCode":"0058"
],
requestContentType : 'application/json' )

def country241 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Vietnam",
"iso2Code":"VN",
"iso3Code":"VNM",
"numericCode":"704",
"phoneCode":"0084"
],
requestContentType : 'application/json' )

def country242 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Virgin Islands",
"iso2Code":"VI",
"iso3Code":"VIR",
"numericCode":"850",
"phoneCode":"1340"
],
requestContentType : 'application/json' )

def country243 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Wallis and Futuna Islands",
"iso2Code":"WF",
"iso3Code":"WLF",
"numericCode":"876",
"phoneCode":"0681"
],
requestContentType : 'application/json' )

def country244 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Western Sahara",
"iso2Code":"EH",
"iso3Code":"ESH",
"numericCode":"732"
],
requestContentType : 'application/json' )

def country245 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Yemen",
"iso2Code":"YE",
"iso3Code":"YEM",
"numericCode":"887",
"phoneCode":"0967"
],
requestContentType : 'application/json' )

def country246 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Zambia",
"iso2Code":"ZM",
"iso3Code":"ZMB",
"numericCode":"894",
"phoneCode":"0260"
],
requestContentType : 'application/json' )

def country247 = luzPersonClient.post( path : COUNTRY_PATH,
body : [
"countryName":"Zimbabwe",
"iso2Code":"ZW",
"iso3Code":"ZWE",
"numericCode":"716",
"phoneCode":"0263"
],
requestContentType : 'application/json' )