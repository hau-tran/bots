package com.axonivy.luz.bots.task;

public class TaskEvent {

	private final Task task;
	
	public TaskEvent(Task task) {
		this.task = task;
	}
	
	public Task task() {
		return task;
	}
	
}
