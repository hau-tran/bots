package com.axonivy.luz.bots.task;

import javax.ws.rs.FormParam;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

public class Task {

	private String id;

	@FormParam("name")
	private String name;

	@FormParam("pollTargetUrl")
	private String pollTargetUrl;

	@FormParam("script")
	@PartType("application/octet-stream")
	private byte[] scriptArchive;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPollTargetUrl() {
		return pollTargetUrl;
	}

	public void setPollTargetUrl(String pollTargetUrl) {
		this.pollTargetUrl = pollTargetUrl;
	}

	public byte[] getScriptArchive() {
		return scriptArchive;
	}

	public void setScriptArchive(byte[] scriptArchive) {
		this.scriptArchive = scriptArchive;
	}

}
