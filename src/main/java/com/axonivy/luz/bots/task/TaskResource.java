package com.axonivy.luz.bots.task;

import java.text.MessageFormat;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

@Path("/tasks")
public class TaskResource {
	
	@Inject
	private TaskRepository taskRepository;
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String add(@MultipartForm Task task) {
		taskRepository.add(task);
		return MessageFormat.format("Received task name [{0}] target [{1}] with file size {2}" , task.getName(), task.getPollTargetUrl(), task.getScriptArchive().length);
	}
	
}
