package com.axonivy.luz.bots.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

@ApplicationScoped
@Default
public class Radar implements TaskRepository {

	@Resource
	private ManagedScheduledExecutorService executorService;
	
	private List<ScheduledFuture<?>> scheduledTasks = new ArrayList<>();
	
	@Inject
	private Event<TaskEvent> taskEventChannel;
	
	@Override
	public String add(Task task) {
		scheduledTasks.add(executorService
				.scheduleAtFixedRate(new UpDownPingTask(task, taskEventChannel), 5, 1, TimeUnit.SECONDS));
		return "TaskReceived";
	}
	
	@PreDestroy
	public void shutdown() {
		scheduledTasks.stream().forEach(s -> s.cancel(true));
	}
	
}
