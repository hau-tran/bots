package com.axonivy.luz.bots.task;

import static com.axonivy.luz.bots.task.UpDownPingTask.LastResult.FAIL;
import static com.axonivy.luz.bots.task.UpDownPingTask.LastResult.SUCCESS;

import javax.enterprise.event.Event;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class UpDownPingTask implements Runnable {

	private final Task task;
	private LastResult lastResult = LastResult.SUCCESS;
	private final Event<TaskEvent> taskEventChannel;

	public UpDownPingTask(Task task, Event<TaskEvent> taskEventChannel) {
		this.task = task;
		this.taskEventChannel = taskEventChannel;
	}
	
	@Override
	public void run() {
		Client client = ClientBuilder.newClient();
		System.out.println("HHH - Prepare to ping " + task.getName() + "to " + task.getPollTargetUrl());
		Response response = client.target(task.getPollTargetUrl())
				.request()
				.get();
		System.out.println("HHH - ping done! ");
		LastResult newResult = isOk(response) ? SUCCESS : FAIL;
		System.out.println("HHH - ping result " + newResult);
		if (newResult == SUCCESS && lastResult == FAIL) {
			taskEventChannel.fire(new TaskEvent(task));
			System.out.println("HHH - event fired!");
		}
		this.lastResult = newResult;
	}
	
	private static boolean isOk(Response response) {
		return Response.Status.OK.getStatusCode() == response.getStatus();
	}
	
	static enum LastResult {
		SUCCESS,
		FAIL;
	}
	
}
