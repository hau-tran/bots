package com.axonivy.luz.bots.task;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;

@Singleton
@LocalBean
public class TaskReactor {

	@Asynchronous
	public void onEvent(@Observes TaskEvent taskEvent) {
		writeTaskEvent(taskEvent);
	}

	private void writeTaskEvent(TaskEvent taskEvent) {
		try {
			Path temporaryFolder = Paths.get(System.getProperty("java.io.tmpdir"));
			Path workspace = temporaryFolder.resolve(Paths.get(taskEvent.task().getName(), "workspace"));
			Files.createDirectories(workspace);
			Path welcome = Paths.get("welcome.txt");
			Files.write(workspace.resolve(welcome), Arrays.asList("Task Received " + taskEvent.task().getName()));
		} catch (IOException failure) {
			System.err.println(failure);
		}
	}
	
	
}
