package com.axonivy.luz.bots.scripting;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.ws.rs.FormParam;

public class ExecuteScriptRequest {

	@FormParam("script")
	private byte[] scriptFile;

	public InputStream getScript() {
		return new ByteArrayInputStream(scriptFile);
	}
	
}
