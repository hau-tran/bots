package com.axonivy.luz.bots.scripting;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.stream.IntStream;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

@Path("script")
public class ScriptingResource {

	@POST
	public Response executeScript(@MultipartForm ExecuteScriptRequest executeScriptRequest) {
		
		StreamingOutput streamingOutput = new StreamingOutput() {
			
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(output)), true);
				out.println(IOUtils.toString(executeScriptRequest.getScript()));
				
				java.nio.file.Path root = Paths.get("d:");
				
				
				try {
					int a = 1/0;
				} catch (Exception e) {
					e.printStackTrace(out);
				}
				
				IntStream.range(0, 10)
					.forEach(i -> {
						
						try {
							Thread.sleep(2000);
						} catch (Exception e) {
							e.printStackTrace(out);
						}
						
						out.println("Step #" + i);
						
					});
				
				
				Files.walkFileTree(root, new FileVisitor<java.nio.file.Path>() {

					@Override
					public FileVisitResult preVisitDirectory(java.nio.file.Path dir, BasicFileAttributes attrs)
							throws IOException {
						out.println(dir.toAbsolutePath().toString());
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult visitFile(java.nio.file.Path file, BasicFileAttributes attrs)
							throws IOException {
						out.println(file.toAbsolutePath().toString());
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace(out);
						}
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult visitFileFailed(java.nio.file.Path file, IOException exc)
							throws IOException {
						exc.printStackTrace(out);
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult postVisitDirectory(java.nio.file.Path dir, IOException exc)
							throws IOException {
						return FileVisitResult.CONTINUE;
					}
				});
				
			}
		};
		
		return Response.ok(streamingOutput).build();
	}
	
}
